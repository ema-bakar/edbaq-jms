package com.katalystdm.edb.edbaq.jms;

import java.sql.Connection;
import java.sql.SQLException;

import com.edb.jdbc.PgConnection;

public class EdbConnectionUtil
{
  public static PgConnection unwrapConnection(Connection connection) throws SQLException
  {
    PgConnection pgconn = null;
    try
    {
      if (connection.isWrapperFor(PgConnection.class))
      {
        pgconn = (PgConnection) connection.unwrap(PgConnection.class);
      }
      else
      {
        throw new SQLException("Incompatible Connection wrapper, unable to find EDB PgConnection.");
      }
    }
    catch (SQLException e)
    {
      throw new SQLException("Unable to unwrap EDB PostgreSQL connection!", e);
    }
    return pgconn;
  }
}
