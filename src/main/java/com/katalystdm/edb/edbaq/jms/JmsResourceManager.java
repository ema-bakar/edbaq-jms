package com.katalystdm.edb.edbaq.jms;

import java.sql.SQLException;
import java.util.Optional;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.sql.DataSource;

public class JmsResourceManager implements AutoCloseable
{

  private JmsResourceHolder jmsResourceHolder;
  private DataSource jmsQueueDS;
  private String queueName;
  private Optional<String> selector;

  public JmsResourceManager(DataSource jmsQueueDS, String queueName)
      throws JMSException, SQLException
  {
    this(jmsQueueDS, queueName, null);
  }

  public JmsResourceManager(DataSource jmsQueueDS, String queueName, String selector)
      throws JMSException, SQLException
  {
    super();
    this.jmsQueueDS = jmsQueueDS;
    this.queueName = queueName;
    this.selector = Optional.ofNullable(selector);
  }

  private JmsResourceHolder getJmsResourceHolder()
  {
    return jmsResourceHolder;
  }

  public void open() throws SQLException, JMSException
  {
    QueueFactoryUtil queueFactoryUtil = new QueueFactoryUtil();

    jmsResourceHolder = queueFactoryUtil.registerSubscription(jmsQueueDS, queueName, selector);
  }

  @Override
  public void close() throws JMSException
  {
    if (getJmsResourceHolder() != null)
    { getJmsResourceHolder().factory().close(); }
  }

  public Message receive() throws JMSException
  {
    validateJmsResourceHolder();
    return getJmsResourceHolder().consumer().receive();
  }

  public Message receive(long timeout) throws JMSException
  {
    validateJmsResourceHolder();
    return getJmsResourceHolder().consumer().receive(timeout);
  }

  public void commit() throws JMSException
  {
    validateJmsResourceHolder();
    getJmsResourceHolder().session().commit();
  }

  private void validateJmsResourceHolder() throws JMSException
  {
    if (getJmsResourceHolder() == null)
    { throw new JMSException("Not initialized."); }
  }

}
