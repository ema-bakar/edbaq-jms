package com.katalystdm.edb.edbaq.jms;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.MessageConsumer;
import javax.jms.Session;

public class JmsResourceHolder
{
  private Connection factory;
  private Session session;
  private Destination destination;
  private MessageConsumer consumer;
  
  public JmsResourceHolder(Connection factory, Session session, Destination destination, MessageConsumer consumer)
  {
    this.factory = factory;
    this.session = session;
    this.destination = destination;
    this.consumer = consumer;
  }

  public Connection factory() { return factory; }
  public Session session() { return session; }
  public Destination destination() { return destination; }
  public MessageConsumer consumer() { return consumer; }
}
