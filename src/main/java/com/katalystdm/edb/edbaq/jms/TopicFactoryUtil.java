package com.katalystdm.edb.edbaq.jms;

import static com.katalystdm.edb.edbaq.jms.EdbConnectionUtil.unwrapConnection;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.sql.SQLException;

import javax.enterprise.util.Nonbinding;
import javax.inject.Qualifier;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.sql.DataSource;

import org.hornetq.jms.client.HornetQTopicConnectionFactory;

import com.edb.jdbc.PgConnection;

public class TopicFactoryUtil
{
  @Qualifier
  @Retention(RUNTIME)
  @Target({METHOD, FIELD, PARAMETER, TYPE})
  public @interface PgTopic 
  {
    @Nonbinding String schema();
    @Nonbinding String topicName();
    @Nonbinding String subscriptionName();
  }
  
  private HornetQTopicConnectionFactory topicConnectionFactory;
  
  public JmsResourceHolder registerSubscription(DataSource jmsQueueDS, String schema, String topicName, String subscriptionName) throws JMSException, SQLException
  {
    PgConnection conn = unwrapConnection(jmsQueueDS.getConnection());
    TopicConnection factory = topicConnectionFactory.createTopicConnection(conn.getUserName(), "");
    TopicSession session = factory.createTopicSession(true, Session.SESSION_TRANSACTED);
    //Topic topic = ((HornetQSession)session).getTopic(schema, topicName);
    Topic topic = session.createTopic(topicName);
    TopicSubscriber subscriber = session.createDurableSubscriber(topic, subscriptionName);
    
    factory.start();

    return new JmsResourceHolder(factory, session, topic, subscriber);
  }
  
}
