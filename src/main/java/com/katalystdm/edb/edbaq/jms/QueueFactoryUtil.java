package com.katalystdm.edb.edbaq.jms;

import static com.katalystdm.edb.edbaq.jms.EdbConnectionUtil.unwrapConnection;

import java.sql.SQLException;
import java.util.Optional;

import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.sql.DataSource;

//import org.hornetq.jms.client.HornetQConnectionFactory;

import com.edb.jdbc.PgConnection;
import com.katalystdm.edb.edbaq.aqapi.EdbQJmsQueueConnectionFactory;

public class QueueFactoryUtil
{
  //private HornetQConnectionFactory connectionFactory;
  private EdbQJmsQueueConnectionFactory connectionFactory;
  
  public JmsResourceHolder registerSubscription(DataSource jmsQueueDS, String queueName, Optional<String> selector) 
      throws JMSException, SQLException
  {
    PgConnection conn = unwrapConnection(jmsQueueDS.getConnection());
    QueueConnection factory = connectionFactory.createQueueConnection(conn, 10);
    QueueSession session = factory.createQueueSession(true, Session.SESSION_TRANSACTED);
    Queue q = session.createQueue(queueName);    
    QueueReceiver receiver = (selector.isPresent()) ? session.createReceiver(q, selector.get()) : session.createReceiver(q);
    factory.start();          
    return new JmsResourceHolder(factory, session, q, receiver);
  }
}