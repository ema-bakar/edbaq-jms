package com.katalystdm.edb.edbaq.aqapi;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.jms.ConnectionConsumer;
import javax.jms.ConnectionMetaData;
import javax.jms.Destination;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueSession;
import javax.jms.ServerSessionPool;
import javax.jms.Session;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicSession;
import javax.sql.DataSource;

public class EdbQJmsConnection extends EdbQJmsObject implements QueueConnection, TopicConnection
{
  private EdbQJmsConnectionMetaData metadata;  
  private EdbQJmsDBConnMgr connMgr = null;
  private EdbQJmsExceptionListener exceptionListener;
  private int connType;
  private String clientID = null;
  private String connectionId;
  
  public EdbQJmsConnection(Connection connection, int conType) throws JMSException
  {
    super();
    this.connMgr = new EdbQJmsDBConnMgr(connection);
    this.init(conType);
  } 
  
  public EdbQJmsConnection(DataSource dataSource, String username, String password, int conType) throws JMSException
  {
    super();
    this.connMgr = new EdbQJmsDBConnMgr(dataSource, username, password);
    this.init(conType);
  }
  
  public EdbQJmsConnection() 
  {
    super();
  }

  @Override
  public Session createSession(boolean transacted, int acknowledgeMode) throws JMSException
  {
    EdbQJmsSession edbQJmsSession = null;
    if (this.isClosed())
    {
      EdbQJmsError.throwIllegalStateEx(114, null);
    }
    EdbQJmsGeneralDBConnection edbQJmsGeneralDBConnection = this.connMgr.getConnection();    
    try
    {
      Connection connection = edbQJmsGeneralDBConnection.getDBConnection();
      if (this.connectionId == null)
      {
        this.setConnId(connection);
      }
      edbQJmsSession = new EdbQJmsSession(this, edbQJmsGeneralDBConnection, transacted, acknowledgeMode, this.connType);
    }
    catch (JMSException e)
    {
      throw e;
    }
    finally
    {
      if (edbQJmsSession == null)
      {
        if (edbQJmsGeneralDBConnection != null)
        {
          edbQJmsGeneralDBConnection.close();
        }
      }
    }
    return edbQJmsSession;
  }

  @Override
  public Session createSession(int sessionMode) throws JMSException
  {
    return null;
  }

  @Override
  public Session createSession() throws JMSException
  {
    return null;
  }

  @Override
  public String getClientID() throws JMSException
  {
    if (this.isClosed())
    {
      EdbQJmsError.throwIllegalStateEx(114, null);
    }
    if (this.clientID == null)
    {
      return this.connMgr.getUserName();
    }
    return this.clientID;
  }

  @Override
  public synchronized void setClientID(String clientID) throws JMSException 
  {
    if (this.isClosed())
    {
      EdbQJmsError.throwIllegalStateEx(114, null);
    }
    this.clientID = clientID;
  }

  @Override
  public synchronized ConnectionMetaData getMetaData() throws JMSException
  {
    if (this.isClosed())
    {
      EdbQJmsError.throwIllegalStateEx(114, null);
    }
    return this.metadata;
  }

  @Override
  public synchronized ExceptionListener getExceptionListener() throws JMSException
  {
    if (this.isClosed())
    {
      EdbQJmsError.throwIllegalStateEx(114, null);
    }    
    if (this.exceptionListener != null)
    {
      return this.exceptionListener.getExceptionListener();
    }
    return null;
  }

  @Override
  public synchronized void setExceptionListener(ExceptionListener listener) throws JMSException 
  {
    if (this.isClosed())
    {
      EdbQJmsError.throwIllegalStateEx(114, null);
    }
    if (this.exceptionListener != null)
    {
      this.exceptionListener.setExceptionListener(listener);
    }
    else
    {
      EdbQJmsGeneralDBConnection edbQJmsGeneralDBConnection = this.connMgr.getConnection();
      this.exceptionListener = new EdbQJmsExceptionListener(edbQJmsGeneralDBConnection);
      this.exceptionListener.setExceptionListener(listener);
    }
  }

  @Override
  public synchronized void start() throws JMSException 
  {
    if (this.exceptionListener != null)
    {
      this.exceptionListener.resumeExceptionListener();
    }
  }

  @Override
  public synchronized void stop() throws JMSException 
  {
    if (this.exceptionListener != null)
    {
      this.exceptionListener.suspendExceptionListener();
    }
  }

  @Override
  public void close() throws JMSException
  {
    super.close();
  }

  @Override
  public synchronized ConnectionConsumer createConnectionConsumer(Destination destination,
      String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException
  {
    EdbQJmsError.throwEx(102);
    return null;
  }

  @Override
  public synchronized ConnectionConsumer createSharedConnectionConsumer(Topic topic, String subscriptionName,
      String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException
  {
    return null;
  }

  @Override
  public synchronized ConnectionConsumer createDurableConnectionConsumer(Topic topic, String subscriptionName,
      String messageSelector, ServerSessionPool sessionPool, int maxMessages) throws JMSException
  {
    EdbQJmsError.throwEx(102);
    return null;
  }

  @Override
  public synchronized ConnectionConsumer createSharedDurableConnectionConsumer(Topic topic,
      String subscriptionName, String messageSelector, ServerSessionPool sessionPool,
      int maxMessages) throws JMSException
  {
    return null;
  }

  @Override
  public QueueSession createQueueSession(boolean transacted, int acknowledgeMode) throws JMSException
  {
    EdbQJmsSession edbQJmsSession = null;
    if (this.isClosed())
    {
      EdbQJmsError.throwIllegalStateEx(114, null);
    }
    EdbQJmsGeneralDBConnection edbQJmsGeneralDBConnection = this.connMgr.getConnection();    
    try
    {
      Connection connection = edbQJmsGeneralDBConnection.getDBConnection();
      if (this.connectionId == null)
      {
        this.setConnId(connection);
      }
      edbQJmsSession = new EdbQJmsSession(this, edbQJmsGeneralDBConnection, transacted, acknowledgeMode, this.connType);
    }
    catch (JMSException e)
    {
      throw e;
    }
    finally
    {
      if (edbQJmsSession == null)
      {
        if (edbQJmsGeneralDBConnection != null)
        {
          edbQJmsGeneralDBConnection.close();
        }
      }
    }
    return edbQJmsSession;
  }

  @Override
  public ConnectionConsumer createConnectionConsumer(Queue queue, String messageSelector,
      ServerSessionPool sessionPool, int maxMessages) throws JMSException
  {
    return null;
  }

  @Override
  public TopicSession createTopicSession(boolean transacted, int acknowledgeMode)
      throws JMSException
  {
    return null;
  }

  @Override
  public ConnectionConsumer createConnectionConsumer(Topic topic, String messageSelector,
      ServerSessionPool sessionPool, int maxMessages) throws JMSException
  {
    return null;
  }

  public String getConnectionId()
  {
    return connectionId;
  }

  public void setConnId(Connection connection) throws JMSException
  {
    Statement statement = null;
    ResultSet resultSet = null;
    try
    {
      statement = connection.createStatement();
      resultSet = statement.executeQuery("select SYS_GUID() from dual");
      resultSet.next();
      this.connectionId = this.BytesToString(resultSet.getBytes(1));
    }
    catch(SQLException e)
    {
      EdbQJmsError.throwEx(122, (Throwable) e);
    }
    finally
    {
      if (resultSet != null)
      {
        try
        {
          resultSet.close();
        }
        catch (SQLException e) {}        
      }
      if (statement != null)
      {
        try
        {
          statement.close();
        }
        catch (SQLException e1) {}
      }      
    }
  }
  
  private void init(int n)
  {
    this.metadata = new EdbQJmsConnectionMetaData();
    this.exceptionListener = null;
    this.connType = n;
  }
  
  private String BytesToString(byte[] arrby) 
  {
    StringBuffer stringBuffer = new StringBuffer();
    for (int i = 0; i < arrby.length; ++i) 
    {
      int n = arrby[i] & 255;
      if (n < 16) 
      {
        stringBuffer.append('0');
      }
      stringBuffer.append(Integer.toHexString(n));
    }
    return stringBuffer.toString();
  }  
}
