package com.katalystdm.edb.edbaq.aqapi;

class EdbQJmsMessageID
{
  private byte[] systemMsgID = null;
  
  private String userSetID = null;
  
  private boolean hasUserSetID = false;  
  
  synchronized String getJMSMessageID() 
  {
    if (this.hasUserSetID)
      return this.userSetID; 
    if (this.systemMsgID != null)
      return "ID:" + bArray2String(this.systemMsgID); 
    return null;
  }
  
  synchronized void setJMSMessageID(String paramString) 
  {
    this.userSetID = paramString;
    this.hasUserSetID = true;
  }  
  
  String bArray2String(byte[] paramArrayOfbyte) 
  {
    StringBuffer stringBuffer = new StringBuffer(paramArrayOfbyte.length * 2);
    for (byte b = 0; b < paramArrayOfbyte.length; b++) {
      stringBuffer.append((char)nibbleToHex((byte)((paramArrayOfbyte[b] & 0xF0) >> 4)));
      stringBuffer.append((char)nibbleToHex((byte)(paramArrayOfbyte[b] & 0xF)));
    } 
    return stringBuffer.toString();
  }
  
  static byte nibbleToHex(byte paramByte) 
  {
    paramByte = (byte)(paramByte & 0xF);
    return (byte)((paramByte < 10) ? (paramByte + 48) : (paramByte - 10 + 65));
  }  
}
