package com.katalystdm.edb.edbaq.aqapi;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;

public class EdbQJmsObject
{
  private static final long CLOSEWAITTIME = 1000L;
  
  private final Object sync = new Object();
  
  private volatile int state = 1;
  
  private String id;
  
  public String getId()
  {
    return id;
  }    
  
  public final boolean isOpen()
  {
    return this.state == 1;
  }
  
  public final boolean isConnOpen()
  {
    return this.state == 1;
  }
  
  public final boolean isClosed()
  {
    return !this.isOpen();
  }
  
  public final boolean isClosing()
  {
    return this.state == 3;
  }
  
  public void markClosed() 
  {
    Object object = this.sync;
    synchronized (object)
    {
      if (this.state == 1)
      {
        this.state = 2;
      }
    }
  }
  
  public void close() throws JMSException
  {
    Object object = this.sync;
    synchronized (object)
    {
      this.markClosed();
      while(this.state == 3)
      {
        try
        {
          object.wait(CLOSEWAITTIME);
        }
        catch (InterruptedException e)
        {
          throw new JMSException("Eroor during close", e.getCause().getMessage());
        }
      }
      if (this.state == 4)
      {
        return;
      }
      this.state = 3;
      this.sync.notifyAll();
    }
    object = null;
  }
  
  public final void checkClosed(String string) throws JMSException
  {
    if (!this.isOpen())
    {
      int n = this instanceof Connection ? 114 : (this instanceof Session ? 131 : (this instanceof MessageProducer ? 138 : (this instanceof MessageConsumer ? 115 : 122)));
      EdbQJmsError.throwIllegalStateEx(n, string + ":" + this.getId());
    }
  }
  
  final void attach(EdbQJmsObject edbQJmsObject) throws JMSException
  {
    Object object = this.sync;
    synchronized (object)
    {
      this.checkClosed("attach");
    }
  }
}
