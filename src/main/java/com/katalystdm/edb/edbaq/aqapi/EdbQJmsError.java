package com.katalystdm.edb.edbaq.aqapi;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.jms.IllegalStateException;
import javax.jms.JMSException;

public class EdbQJmsError
{
  private static ResourceBundle bundle;
  public static final int INVALID_DELIVERY_MODE = 101;
  public static final int NOT_SUPPORTED = 102;
  public static final int SUB_IMPL = 103;
  public static final int PAYLOAD_NULL = 104;
  public static final int AGENT_NULL = 105;
  public static final int MULTI_SESSION = 106;
  public static final int INVALID_OPERATION = 107;
  public static final int INVALID_MESSAGE_TYPE = 108;
  public static final int CLASS_NOT_FOUND = 109;
  public static final int FIELD_NOT_WRITEABLE = 110;
  public static final int CONN_NULL = 111;
  public static final int INVALID_CONN = 112;
  public static final int CONN_STOPPED = 113;
  public static final int CONN_CLOSED = 114;
  public static final int CONSUMER_CLOSED = 115;
  public static final int INVALID_CONSUMER = 116;
  public static final int CONVERSION_FAILED = 117;
  public static final int INVALID_VALUE = 118;
  public static final int INVALID_PROP_VALUE = 119;
  public static final int DEQUEUE_FAILED = 120;
  public static final int DEST_PROP_NULL = 121;
  public static final int INTERNAL_ERROR = 122;
  public static final int INVALID_INTERVAL = 123;
  public static final int INVALID_DEQ_MODE = 124;
  public static final int INVALID_QUEUE = 125;
  public static final int INVALID_TOPIC = 126;
  public static final int INVALID_DESTINATION = 127;
  public static final int INVALID_NAVIG_MODE = 128;
  public static final int INVALID_PAYLOAD_TYPE = 129;
  public static final int MULTICONS_QUEUE = 130;
  public static final int SESSION_CLOSED = 131;
  public static final int MAX_PROP_EXCEEDED = 132;
  public static final int MESSAGE_NULL = 133;
  public static final int NAME_NULL = 134;
  public static final int INVALID_DRIVER = 135;
  public static final int PLOAD_FACT_NOTNULL = 136;
  public static final int PLOAD_FACT_NULL = 137;
  public static final int PRODUCER_CLOSED = 138;
  public static final int PROP_NAME_NULL = 139;
  public static final int INVALID_PROP_NAME = 140;
  public static final int INVALID_QTABLE = 141;
  public static final int MULTICONS_NOT_ENABLED = 142;
  public static final int QUEUE_NULL = 143;
  public static final int MULTICONS_ENABLED = 144;
  public static final int INVALID_RECP_LIST = 145;
  public static final int REGISTRATION_FAILED = 146;
  public static final int INVALID_REPLYTO = 147;
  public static final int MAX_PROP_SIZE_EXCEEDED = 148;
  public static final int SUBS_NULL = 149;
  public static final int PROP_NOT_SUPPORTED = 150;
  public static final int INVALID_TOPIC_TYPE = 151;
  public static final int INVALID_ACCESS_MODE = 152;
  public static final int INVALID_PROP_TYPE = 153;
  public static final int INVALID_SEQ_DEV = 154;
  public static final int AQ_EXCEPTION = 155;
  public static final int INVALID_CLASS = 156;
  public static final int IO_EXCEPTION = 157;
  public static final int SQL_EXCEPTION = 158;
  public static final int INVALID_SELECTOR = 159;
  public static final int EOF_EXCEPTION = 160;
  public static final int MESG_FORMAT_EXCEPTION = 161;
  public static final int MESG_NOT_READABLE = 162;
  public static final int MESG_NOT_WRITEABLE = 163;
  public static final int NO_SUCH_ELEMENT = 164;
  public static final int MAX_VALUE_SIZE_EXCEEDED = 165;
  public static final int TOPIC_NULL = 166;
  public static final int INVALID_DEQ_PARAMS = 167;
  public static final int MULTI_DEQ_PARAMS = 168;
  public static final int SQL_DATA_CLASS_NULL = 169;
  public static final int INVALID_REL_MSGID = 170;
  public static final int INVALID_MSG_PAYLOAD = 171;
  public static final int MULTI_QUEUE_TABLES = 172;
  public static final int QTABLE_NOT_FOUND = 173;
  public static final int CLASS_NULL = 174;
  public static final int DEQ_OPTION_NULL = 175;
  public static final int ENQ_OPTION_NULL = 176;
  public static final int INVALID_DEQ_CALL = 177;
  public static final int INVALID_QUEUE_NAME = 178;
  public static final int INVALID_QTABLE_NAME = 179;
  public static final int INVALID_QUEUE_TYPE = 180;
  public static final int INVALID_WAIT_TIME = 181;
  public static final int MULTI_QUEUE = 182;
  public static final int NO_AQ_DRIVER = 183;
  public static final int INVALID_QUEUE_HANDLE = 184;
  public static final int QUEUE_PROP_NULL = 185;
  public static final int QTABLE_PROP_NULL = 186;
  public static final int QTABLE_NULL = 187;
  public static final int INVALID_QTABLE_HANDLE = 188;
  public static final int BYTE_ARRAY_SMALL = 189;
  public static final int QUEUE_NOT_FOUND = 190;
  public static final int INVALID_SQLDATA_CLASS = 191;
  public static final int INVALID_VISIBILITY = 192;
  public static final int RAW_NOT_ALLOWED = 193;
  public static final int INVALID_SESSION = 194;
  public static final int INVALID_OBJ_TYPE = 195;
  public static final int MULTI_BROWSER = 196;
  public static final int AGENT_ADDRESS_NULL = 197;
  public static final int PRIVILEGED_LISTENER_SET = 198;
  public static final int NOTIFICATION_REG_FAILED = 199;
  public static final int DESTINATION_NULL = 200;
  public static final int RECIPIENT_NULL = 201;
  public static final int NOTIFICATION_UNREG_FAILED = 202;
  public static final int PAYLOAD_FACTORY_NULL = 203;
  public static final int JNI_ERROR = 204;
  public static final int NAMING_EXCEPTION = 205;
  public static final int XA_EXCEPTION = 206;
  public static final int JMS_EXCEPTION = 207;
  public static final int XSQL_EXCEPTION = 208;
  public static final int SAX_EXCEPTION = 209;
  public static final int XMLPARSE_EXCEPTION = 210;
  public static final int CONN_NOT_AVAILABLE = 220;
  public static final int FREE_CONN_NOT_AVAILABLE = 221;
  public static final int INVALID_PLOAD_FACT_TYPE = 222;
  public static final int ANYQ_PLOAD_FACT_NOTNULL = 223;
  public static final int ANYQ_TYPEMAP_INVALID = 224;
  public static final int ANYQ_INVALID_DRIVER = 225;
  public static final int MESG_NO_BODY = 226;
  public static final int NON_TRANS_COMMIT = 227;
  public static final int NON_TRANS_ROLLBACK = 228;
  public static final int JMS_PARAM_NULL = 229;
  public static final int ACTIVE_DURABLE_SUBS_EXIST = 230;
  public static final int INVALID_TEMP_DEST = 231;
  public static final int SECURITY_EXCEPTION = 232;
  public static final int SUBS_INFO_UNAVAILABLE = 233;
  public static final int WRONG_DOMAIN_OPERATION = 234;
  public static final int UNKNOWN_SUB_NAME = 235;
  public static final int INVALID_OCI_HANDLE = 236;
  public static final int THREAD_START_FAILURE = 237;
  public static final int TRANS_RECOVER = 238;
  public static final int TRANS_IN_PROGRESS = 239;
  public static final int CLIENT_ID_ERROR = 240;
  public static final int CONS_IN_TEMP_DEST = 241;
  public static final int VISIBILITY_CONFLICT = 242;
  public static final int TOPIC_NOT_FOUND = 243;
  public static final int INVALID_OPERATION_SHARDED_QUEUE = 244;
  public static final int STREAMING_SHARDED_QUEUE_ONLY = 245;
  public static final int STREAMING_INVALID_DRIVER = 246;
  public static final int STREAMING_PERSISTENT_MESSAGE_DELIVERY_ONLY = 247;
  public static final int STREAMING_DISABLED = 248;
  public static final int INPUTSTREAM_NULL = 249;
  public static final int OUTSTREAM_NULL = 250;
  public static final int INVALID_WRITE_WITH_STREAMING = 251;
  public static final int INVALID_READ_WITH_STREAMING = 252;
  public static final int INVALID_OPERATION_WITH_NULL_MESSAGEID = 253;
  public static final int DEQUEUE_WITHOUT_STREAMING = 254;
  public static final int STREAMING_INVALID_SESSION_MODE = 255;
  public static final int CONNECTION_STOP_TIMEDOUT = 256;
  public static final int NETWORK_TIMEDOUT = 257;  
  
  public static String getMessage(String string, Object[] arrobject) 
  {
    String string2 = null;
    if (bundle == null) 
    {
      bundle = ResourceBundle.getBundle("com.katalystdm.edb.edbaq.aqapi.EdbQJmsMessages");
    }
    try 
    {
      string2 = MessageFormat.format(bundle.getString(string), arrobject);
      string2 = "JMS-" + string + ": " + string2;
    }
    catch (Exception exception) 
    {
      string2 = "Message [" + string + "] not found in 'EdbQJmsMessages'.";
    }
    return string2;
  }

  public static String getMessage(int n) throws JMSException 
  {
    Object[] arrobject = new String[]{"", ""};
    return EdbQJmsError.getMessage(Integer.toString(n), arrobject);
  }  
  
  public static void throwEx(int n) throws JMSException 
  {
    Object[] arrobject = new String[]{"", ""};
    String string = EdbQJmsError.getMessage(Integer.toString(n), arrobject);
    throw new JMSException(string, Integer.toString(n));
  }

  public static void throwEx(String string, Throwable throwable) throws JMSException 
  {
    String errorCode = null;
    if (throwable instanceof Exception)
    {
      errorCode =  throwable.getCause().toString();
    }
    throw new JMSException(string, errorCode);
  }

  public static void throwEx(int n, Throwable throwable) throws JMSException 
  {
    Object[] arrobject = new String[]{"", ""};
    String string = EdbQJmsError.getMessage(Integer.toString(n), arrobject);
    String errorCode = null;
    if (throwable instanceof Exception)
    {
      errorCode =  throwable.getCause().toString();
    }
    throw new JMSException(string, errorCode);    
  }
  
  public static void throwEx(int n, String string) throws JMSException 
  {
    Object[] arrobject = new String[]{string == null ? "" : string, ""};
    String string2 = EdbQJmsError.getMessage(Integer.toString(n), arrobject);
    throw new JMSException(string2, Integer.toString(n));
  }
  
  public static void throwEx(int n, String string, Throwable throwable) throws JMSException 
  {
    Object[] arrobject = new String[]{string == null ? "" : string, ""};
    String string2 = EdbQJmsError.getMessage(Integer.toString(n), arrobject);
    String errorCode = null;
    if (throwable instanceof Exception)
    {
      errorCode =  throwable.getCause().toString();
    }
    throw new JMSException(string2, errorCode);        
  }
  
  public static void throwEx(int n, String string, String string2) throws JMSException 
  {
    Object[] arrobject = new String[]{string == null ? "" : string, string2 == null ? "" : string2};
    String string3 = EdbQJmsError.getMessage(Integer.toString(n), arrobject);
    throw new JMSException(string3, Integer.toString(n));
  }
  
  public static void throwEx(int n, String string, String string2, Throwable throwable) throws JMSException 
  {
    Object[] arrobject = new String[]{string == null ? "" : string, string2 == null ? "" : string2};
    String string3 = EdbQJmsError.getMessage(Integer.toString(n), arrobject);
    String errorCode = null;
    if (throwable instanceof Exception)
    {
      errorCode =  throwable.getCause().toString();
    }
    throw new JMSException(string3, errorCode);       
  }
  
  public static void throwIllegalStateEx(int n, String string) throws JMSException
  {
    Object[] arrobject = new String[]{string == null ? "" : string, ""};
    String string2 = EdbQJmsError.getMessage(Integer.toString(n), arrobject);
    throw new IllegalStateException(string2, Integer.toString(n));
  }
}
