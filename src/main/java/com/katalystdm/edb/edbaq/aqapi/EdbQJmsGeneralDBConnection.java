package com.katalystdm.edb.edbaq.aqapi;

import java.sql.Connection;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.sql.XAConnection;

public class EdbQJmsGeneralDBConnection
{
  private EdbQJmsDBConnMgr connMgr = null;
  private Connection conn = null;
  private XAConnection xaConn = null;
  
  public EdbQJmsGeneralDBConnection(EdbQJmsDBConnMgr connMgr, Connection conn)
  {
    this.connMgr = connMgr;
    this.conn = conn;
  }

  public EdbQJmsGeneralDBConnection(EdbQJmsDBConnMgr connMgr, XAConnection xaConn)
  {
    this.connMgr = connMgr;
    this.xaConn = xaConn;
  }
  
  public Connection getDBConnection() throws JMSException
  {
    if (this.conn == null)
    {
      try
      {
        this.conn = this.xaConn.getConnection();
      }
      catch (SQLException e)
      {
        throw new JMSException(e.getCause().getMessage());
      }
    }
    return this.conn;
  }
  
  public XAConnection getXAConnection()
  {
    return this.xaConn;
  }
  
  public void close() throws JMSException
  {
    Throwable throwable = null;
    try 
    {
      if (this.conn != null) 
      {
        this.conn.close();
      }
    }
    catch (Throwable throwable2) 
    {
      throwable = throwable2;
    }
    try 
    {
      if (this.xaConn != null) 
      {
        this.xaConn.close();
      }
    }
    catch (Throwable throwable3) 
    {
      throwable = throwable3;
    }
    this.conn = null;
    this.xaConn = null;
    this.connMgr = null;    
    if (throwable != null)
    {
      EdbQJmsError.throwEx("Error in close EdbQJmsGeneralDBConnection", throwable);
    }
  }
}
