package com.katalystdm.edb.edbaq.aqapi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;

public class EdbQJmsExceptionListener extends Thread implements ExceptionListener
{
  private static final String PING_QUERY = "SELECT banner FROM v$version where 1<>1";
  
  private boolean suspended = true;
  private Connection conn;
  private EdbQJmsGeneralDBConnection genConn;
  private ExceptionListener el;
  private PreparedStatement pstmt;

  public EdbQJmsExceptionListener(EdbQJmsGeneralDBConnection genConn) throws JMSException
  {
    this.genConn = genConn;
    this.conn = genConn.getDBConnection();
    try
    {
      this.pstmt = this.conn.prepareStatement(PING_QUERY);
      this.pstmt.setQueryTimeout(1);
    }
    catch (SQLException e)
    {
      throw new JMSException(e.getCause().getMessage());
    }
  }

  @Override
  public synchronized void onException(JMSException exception)
  {
    if (this.el != null)
    {
      try
      {
        this.el.onException(exception);
      }
      catch (RuntimeException re)
      {
        throw re; 
      }
      catch (Error e)
      {
        throw e;
      }
    }    
  }

  public ExceptionListener getExceptionListener()
  {
    return el;
  }

  public synchronized void setExceptionListener(ExceptionListener el)
  {
    this.el = el;
  }
  
  public synchronized void resumeExceptionListener()
  {
    if (this.suspended)
    {
      this.suspended = false;
    }
  }
  
  public synchronized void suspendExceptionListener()
  {
    this.suspended = true;
  }
}
