package com.katalystdm.edb.edbaq.aqapi;

import java.io.Serializable;
import java.sql.Connection;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TemporaryTopic;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;

public class EdbQJmsSession extends EdbQJmsObject implements QueueSession, TopicSession
{
  private boolean transacted;
  private Connection dbConn;
  private EdbQJmsConnection jmsConnection;
  private EdbQJmsGeneralDBConnection genDbConn;
  private int ackMode;
  private int connType;
  private Thread closingThread = null;
  
  public EdbQJmsSession(EdbQJmsConnection jmsConnection, EdbQJmsGeneralDBConnection genDbConn, 
      boolean transacted, int ackMode, int connType) throws JMSException
  {
    super();
    this.jmsConnection = jmsConnection;
    this.genDbConn = genDbConn;
    this.dbConn = genDbConn.getDBConnection();
    this.transacted = transacted;
    this.ackMode = ackMode;
    this.connType = connType;
    if (connType != 10 && connType !=20)
    {
      EdbQJmsError.throwEx(122, "invalid type");
    }
    jmsConnection.attach(this);
  }

  @Override
  public BytesMessage createBytesMessage() throws JMSException
  {
    EdbQJmsBytesMessage edbQJmsBytesMessage = null;
    this.checkSessionStarted();
    edbQJmsBytesMessage = new EdbQJmsBytesMessage(this);
    return edbQJmsBytesMessage;
  }

  @Override
  public MapMessage createMapMessage() throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Message createMessage() throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public ObjectMessage createObjectMessage() throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public ObjectMessage createObjectMessage(Serializable object) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public StreamMessage createStreamMessage() throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public TextMessage createTextMessage() throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public TextMessage createTextMessage(String text) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public boolean getTransacted() throws JMSException
  {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public int getAcknowledgeMode() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public void commit() throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void rollback() throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void close() throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void recover() throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public MessageListener getMessageListener() throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void setMessageListener(MessageListener listener) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void run()
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public MessageProducer createProducer(Destination destination) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public MessageConsumer createConsumer(Destination destination) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public MessageConsumer createConsumer(Destination destination, String messageSelector)
      throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public MessageConsumer createConsumer(Destination destination, String messageSelector,
      boolean noLocal) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public MessageConsumer createSharedConsumer(Topic topic, String sharedSubscriptionName)
      throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public MessageConsumer createSharedConsumer(Topic topic, String sharedSubscriptionName,
      String messageSelector) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Topic createTopic(String topicName) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public TopicSubscriber createDurableSubscriber(Topic topic, String name) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public TopicSubscriber createDurableSubscriber(Topic topic, String name, String messageSelector,
      boolean noLocal) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public MessageConsumer createDurableConsumer(Topic topic, String name) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public MessageConsumer createDurableConsumer(Topic topic, String name, String messageSelector,
      boolean noLocal) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public MessageConsumer createSharedDurableConsumer(Topic topic, String name) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public MessageConsumer createSharedDurableConsumer(Topic topic, String name,
      String messageSelector) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public TemporaryTopic createTemporaryTopic() throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public void unsubscribe(String name) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public TopicSubscriber createSubscriber(Topic topic) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public TopicSubscriber createSubscriber(Topic topic, String messageSelector, boolean noLocal)
      throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public TopicPublisher createPublisher(Topic topic) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Queue createQueue(String queueName) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public QueueReceiver createReceiver(Queue queue) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public QueueReceiver createReceiver(Queue queue, String messageSelector) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public QueueSender createSender(Queue queue) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public QueueBrowser createBrowser(Queue queue) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public QueueBrowser createBrowser(Queue queue, String messageSelector) throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public TemporaryQueue createTemporaryQueue() throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }
  
  boolean isSessionClosed() throws JMSException
  {
    boolean b = false;
    if (this.isClosed())
    {
      return true;
    }
    
    return b;
  }
  
  private void checkSessionStarted() throws JMSException
  {
    if (this.isSessionClosed() && Thread.currentThread() != this.closingThread)
    {
      EdbQJmsError.throwIllegalStateEx(131, null);
    }
  }
}
