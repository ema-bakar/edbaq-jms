package com.katalystdm.edb.edbaq.aqapi;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Enumeration;

import javax.jms.BytesMessage;
import javax.jms.Destination;
import javax.jms.JMSException;

public class EdbQJmsBytesMessage extends EdbQJmsMessage implements BytesMessage
{
  private byte[] bytesData = new byte[0];
  private ByteArrayOutputStream byteArrayOutputStream;
  private DataInputStream dataInputStream;
  private DataOutputStream dataOutputStream;
  
  public EdbQJmsBytesMessage()
  {
    super();
    this.byteArrayOutputStream = new ByteArrayOutputStream();
    this.dataOutputStream = new DataOutputStream(byteArrayOutputStream);
  }
  
  public EdbQJmsBytesMessage(EdbQJmsSession edbQJmsSession)
  {
    super(edbQJmsSession);
    this.byteArrayOutputStream = new ByteArrayOutputStream();
    this.dataOutputStream = new DataOutputStream(byteArrayOutputStream);    
  }

  public EdbQJmsBytesMessage(EdbQJmsSession edbQJmsSession, byte[] bytesData)
  {
    super(edbQJmsSession);
    if (bytesData != null)
    {
      this.bytesData = bytesData;
    }
    this.dataInputStream = new DataInputStream(dataInputStream);
  }

  @Override
  public long getBodyLength() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public boolean readBoolean() throws JMSException
  {
    // TODO Auto-generated method stub
    return false;
  }

  @Override
  public byte readByte() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int readUnsignedByte() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public short readShort() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int readUnsignedShort() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public char readChar() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int readInt() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public long readLong() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public float readFloat() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public double readDouble() throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public String readUTF() throws JMSException
  {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public int readBytes(byte[] value) throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public int readBytes(byte[] value, int length) throws JMSException
  {
    // TODO Auto-generated method stub
    return 0;
  }

  @Override
  public void writeBoolean(boolean value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeByte(byte value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeShort(short value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeChar(char value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeInt(int value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeLong(long value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeFloat(float value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeDouble(double value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeUTF(String value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeBytes(byte[] value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeBytes(byte[] value, int offset, int length) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void writeObject(Object value) throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void reset() throws JMSException
  {
    // TODO Auto-generated method stub
    
  }

}
