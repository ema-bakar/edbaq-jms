package com.katalystdm.edb.edbaq.aqapi;

import java.io.Serializable;
import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.sql.DataSource;

import com.edb.jdbc.PgConnection;

public class EdbQJmsQueueConnectionFactory extends EdbQJmsConnectionFactory implements QueueConnectionFactory, Serializable
{  
  public EdbQJmsQueueConnectionFactory(DataSource dataSource, Properties properties, int connType)
  {
    this.dataSource = dataSource;
    this.connType = connType;
    this.connection = null;
    this.className = null;
    this.username = null;
    this.password = null;
    this.properties = properties;    
  }
  
  public EdbQJmsQueueConnectionFactory(PgConnection connection, int connType)
  {
    this.dataSource = null;
    this.connType = connType;
    this.connection = connection;
    this.className = null;
    this.username = null;
    this.password = null;
    this.properties = null;    
  }  
  
  public EdbQJmsQueueConnectionFactory() {}

  @Override
  public QueueConnection createQueueConnection() throws JMSException
  {
    EdbQJmsConnection jmsConnection = null;
    if (this.connType != 10)
    {
      throw new JMSException("Connection is not a Query connection.");
    }    
    if (this.dataSource != null && this.properties != null)
    {
      jmsConnection = new EdbQJmsConnection(this.dataSource, getUsername(), getPassword(), this.connType);
    }
    else if (this.connection != null)
    {
      jmsConnection = new EdbQJmsConnection(this.connection, this.connType);
    }
    return jmsConnection;
  }

  @Override
  public QueueConnection createQueueConnection(String userName, String password) throws JMSException
  {
    EdbQJmsConnection jmsConnection = null;
    if (this.dataSource != null)
    {
      jmsConnection = new EdbQJmsConnection(this.dataSource, userName, password, this.connType); 
    }
    return jmsConnection;
  }

  public QueueConnection createQueueConnection(PgConnection pgConnection, int connType) throws JMSException
  {
    EdbQJmsConnection edbQJmsConnection = null;
    edbQJmsConnection = new EdbQJmsConnection(pgConnection, connType);
    return edbQJmsConnection;
  }
}
