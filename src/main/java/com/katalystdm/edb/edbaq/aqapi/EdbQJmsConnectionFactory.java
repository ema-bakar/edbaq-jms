package com.katalystdm.edb.edbaq.aqapi;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.Properties;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;
import javax.sql.DataSource;

import com.edb.jdbc.PgConnection;

public class EdbQJmsConnectionFactory implements ObjectFactory, ConnectionFactory, Serializable
{
  DataSource dataSource;  
  int connType;
  PgConnection connection;  
  Properties properties;  
  String className;
  String username;
  String password;
  
  public EdbQJmsConnectionFactory(DataSource dataSource, Properties properties, int connType)
  {
    this.dataSource = dataSource;
    this.connType = connType;
    this.connection = null;
    this.className = null;
    this.username = null;
    this.password = null;
    this.properties = properties;
  }
  
  public EdbQJmsConnectionFactory(PgConnection connection, int connType)
  {
    this.dataSource = null;
    this.connType = connType;
    this.connection = connection;
    this.className = null;
    this.username = null;
    this.password = null;
    this.properties = null;
  }  
  
  public EdbQJmsConnectionFactory()
  {
    this.dataSource = null;
    this.connType = 0;
    this.connection = null;
    this.className = null;
    this.username = null;
    this.password = null;
    this.properties = null;
  }

  @Override
  public Connection createConnection() throws JMSException
  {
    EdbQJmsConnection edbQJmsConnection = null;
    if (this.connType != 10 || this.connType != 20)
    {
      throw new JMSException("Connection is not a Query / Topic connection.");
    }
    if (this.dataSource != null && this.properties != null)
    {
      edbQJmsConnection = new EdbQJmsConnection(this.dataSource, getUsername(), getPassword(), this.connType);
    }
    else if (this.connection != null)
    {
      edbQJmsConnection = new EdbQJmsConnection(this.connection, this.connType);
    }
    return edbQJmsConnection;
  }

  @Override
  public Connection createConnection(String userName, String password) throws JMSException
  {
    EdbQJmsConnection edbQJmsConnection = null;
    if (this.dataSource != null && 
        (userName != null && !userName.equals("")) && 
        (password != null && !password.equals("")))
    {
      edbQJmsConnection = new EdbQJmsConnection(this.dataSource, userName, password, this.connType);
    }
    return edbQJmsConnection;
  }

  @Override
  public Object getObjectInstance(Object arg0, Name arg1, Context arg2, Hashtable<?, ?> arg3)
      throws Exception
  {
    Reference reference = (Reference) arg0;
    if (reference == null)
    {
      return null;
    }
    this.className = reference.getClassName();
    if (this.properties == null && 
        (this.username != null && !this.username.equals(""))  && 
        (this.password != null && !this.password.equals("")))
    {
      this.properties = new Properties();
      this.properties.put("user", this.username);
      this.properties.put("password", this.password);
    }
    if (this.className.equals("com.katalystdm.edb.edbaq.aqapi.EdbQJmsQueueConnectionFactory"))
    {
      if (this.dataSource != null && this.properties != null)
      {
        return new EdbQJmsQueueConnectionFactory(this.dataSource, this.properties, this.connType);
      }
      else if (this.connection != null)
      {
        return new EdbQJmsQueueConnectionFactory(this.connection, this.connType);
      }
    }
    else if (this.className.equals("com.katalystdm.edb.edbaq.aqapi.EdbQJmsConnectionFactory"))
    {
      if (this.dataSource != null && this.properties != null)
      {
        return new EdbQJmsConnectionFactory(this.dataSource, this.properties, this.connType);
      }
      else if (this.connection != null)
      {
        return new EdbQJmsConnectionFactory(this.connection, this.connType);
      }
    }
    return null;
  }
  
  public Connection createConnection(PgConnection connection, int connType) throws JMSException
  {
    EdbQJmsConnection edbQJmsConnection = null;
    edbQJmsConnection = new EdbQJmsConnection(connection, connType);
    return edbQJmsConnection;
  }

  @Override
  public JMSContext createContext()
  {
    return null;
  }

  @Override
  public JMSContext createContext(String userName, String password)
  {
    return null;
  }

  @Override
  public JMSContext createContext(String userName, String password, int sessionMode)
  {
    return null;
  }

  @Override
  public JMSContext createContext(int sessionMode)
  {
    return null;
  }

  public DataSource getDataSource()
  {
    return dataSource;
  }

  public void setDataSource(DataSource dataSource)
  {
    this.dataSource = dataSource;
  }

  public PgConnection getConnection()
  {
    return connection;
  }

  public void setConnection(PgConnection connection)
  {
    this.connection = connection;
  }

  public String getUsername()
  {
    if (this.properties == null || 
        this.properties.get("user") ==  null)
    {
      return null;
    }
    return (String) this.properties.get("user");
  }

  public String getPassword()
  {
    if (this.properties == null || 
        this.properties.get("password") == null)
    {
      return null;
    }
    return (String) this.properties.get("password");
  }  
}
