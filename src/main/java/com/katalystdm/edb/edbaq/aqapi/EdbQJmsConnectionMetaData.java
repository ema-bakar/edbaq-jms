package com.katalystdm.edb.edbaq.aqapi;

import java.util.Enumeration;

import javax.jms.ConnectionMetaData;
import javax.jms.JMSException;

public class EdbQJmsConnectionMetaData implements ConnectionMetaData
{
  static String providerName = "EDB";
  
  static String providerVersion = "42.2.19.1";
  
  static int providerMajorVersion = 42;
  
  static int providerMinorVersion = 2;
  
  static String jmsVersion = "1.1";
  
  static int jmsMajorVersion = 1;
  
  static int jmsMinorVersion = 1;
  
  @Override
  public String getJMSVersion() throws JMSException
  {
    return jmsVersion;
  }

  @Override
  public int getJMSMajorVersion() throws JMSException
  {
    return jmsMajorVersion;
  }

  @Override
  public int getJMSMinorVersion() throws JMSException
  {
    return jmsMinorVersion;
  }

  @Override
  public String getJMSProviderName() throws JMSException
  {
    return providerName;
  }

  @Override
  public String getProviderVersion() throws JMSException
  {
    return providerVersion;
  }

  @Override
  public int getProviderMajorVersion() throws JMSException
  {
    return providerMajorVersion;
  }

  @Override
  public int getProviderMinorVersion() throws JMSException
  {
    return providerMinorVersion;
  }

  @Override
  public Enumeration getJMSXPropertyNames() throws JMSException
  {
    return EdbQJmsMessage.mSystemProperties.keys();
  }
}
