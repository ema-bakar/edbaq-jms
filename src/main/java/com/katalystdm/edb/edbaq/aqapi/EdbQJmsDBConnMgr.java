package com.katalystdm.edb.edbaq.aqapi;

import java.sql.Connection;
import java.sql.SQLException;

import javax.jms.JMSException;
import javax.jms.JMSSecurityException;
import javax.sql.DataSource;
import javax.sql.XADataSource;

import com.edb.jdbc.PgConnection;

public class EdbQJmsDBConnMgr
{
  private DataSource dataSource;
  private XADataSource xaDataSource;
  private EdbQJmsGeneralDBConnection conn;
  private String dbversion;
  private String user;
  private String password;
  
  public EdbQJmsDBConnMgr(Connection connection) throws JMSException
  {
    this.extraInit(connection);
  }
  
  public EdbQJmsDBConnMgr(DataSource dataSource, String userName, String password) throws JMSException
  {
    this.dataSource = dataSource;
    this.user = userName;
    this.password = password;
    this.conn = this.getConnection();
  }  
  
  public EdbQJmsGeneralDBConnection getConnection() throws JMSException
  {
    EdbQJmsGeneralDBConnection edbQJmsGeneralDBConnection = null;
    if (this.conn != null)
    {
      edbQJmsGeneralDBConnection = this.conn;
      this.conn = null;
      return edbQJmsGeneralDBConnection;
    }
    try
    {
      if (this.xaDataSource != null)
      {
        edbQJmsGeneralDBConnection = this.user != null && this.password != null ? 
            new EdbQJmsGeneralDBConnection(this, this.xaDataSource.getXAConnection(this.user, this.password)) : 
              new EdbQJmsGeneralDBConnection(this, this.xaDataSource.getXAConnection());
      }
      else if (this.dataSource != null)
      {
        edbQJmsGeneralDBConnection = this.user != null && this.password != null ? 
            new EdbQJmsGeneralDBConnection(this, this.dataSource.getConnection(this.user, this.password)) : 
              new EdbQJmsGeneralDBConnection(this, this.dataSource.getConnection());    
      }
      this.extraInit(edbQJmsGeneralDBConnection.getDBConnection());
    }
    catch (SQLException e)
    {
      if (edbQJmsGeneralDBConnection != null)
      {
        edbQJmsGeneralDBConnection.close();        
      }
      if (e.getErrorCode() == 1017 || e.getErrorCode() == 17079)
      {
        throw new JMSSecurityException(EdbQJmsError.getMessage(232));
      }
      throw new JMSException(e.getCause().getMessage());      
    }
    catch (Exception ex)
    {
      if (edbQJmsGeneralDBConnection != null)
      {
        edbQJmsGeneralDBConnection.close();        
      }
      throw new JMSException("Error creating the DB connection", ex.getCause().getMessage());
    }
    return edbQJmsGeneralDBConnection;
  }
  
  public String getUserName()
  {
    return this.user;
  }
  
  public void close() throws JMSException
  {
    Exception exception = null;
    
    this.conn = null;
    if (exception != null)
    {
      throw new JMSException("Error in close", exception.getCause().getMessage());
    }
  }
  
  private void extraInit(Connection connection) throws JMSException
  {
    if (connection == null)
    {
      EdbQJmsError.throwEx(111);
    }
    if (!(connection instanceof PgConnection))
    {
      EdbQJmsError.throwEx(112);
    }
    if (this.dbversion == null || this.dbversion.equals(""))
    {
      this.dbversion = ((PgConnection) connection).getDBVersionNumber();
    }      
    if (this.user == null)
    {
      try
      {
        this.user = ((PgConnection) connection).getUserName();
      }
      catch (SQLException e)
      {
        if (e.getErrorCode() == 1017 || e.getErrorCode() == 17079)
        {
          throw new JMSSecurityException(EdbQJmsError.getMessage(232));
        }
        throw new JMSException(e.getCause().getMessage());
      }
    }      
  }
}
